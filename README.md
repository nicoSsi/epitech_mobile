## Nom du projet
epitechmobilehybride
## Description du projet
epitechmobilehybride , Application auto générée par l'outil Southside
## A propos
Projet autogénéré le 2019-9-28
## Sécurité
Le projet est sécurisé grâce à Auth0 avec une implémentation du protocole Oauth2.
Adresses de callback valides:
com.epitech.test://southside.eu.auth0.com/cordova/com.epitech.test/callback 
## Installation 
npm install
## Utilisation
ENV=dev || prod ionic cordova build ios || android