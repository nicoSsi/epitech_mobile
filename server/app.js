var express = require('express');
var app = express();
var jwt = require('express-jwt');
var jwks = require('jwks-rsa');

var port = process.env.PORT || 8769;


//Middleware

//Auth0
var jwtCheck = jwt({
      secret: jwks.expressJwtSecret({
          cache: true,
          rateLimit: true,
          jwksRequestsPerMinute: 5,
          jwksUri: 'https://southside.eu.auth0.com/.well-known/jwks.json'
    }),
    audience: 'https://ssistarter.southside-interactive.io',
    issuer: 'https://southside.eu.auth0.com/',
    algorithms: ['RS256']
});

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});



//On choisit les fonctions sur lesquelles appliquer le middleware
app.use(['/api/private'], jwtCheck);

app.get('/api/public', function (req, res) {
    res.send('{"message": "Hello from a public endpoint! You need to be authenticated to see this."}');
});

app.get('/api/private', function (req, res) {
    res.send('{"message": "Hello from a private endpoint! You need to be authenticated to see this."}');
});

app.listen(port);