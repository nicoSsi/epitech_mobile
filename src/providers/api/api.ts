import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ENV } from '@app/env';
/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }


async publicCall(): Promise<any> {
  alert('call');
  return this.http
    .get(ENV.urlendpoint +'/api/public')
    .toPromise();
}

async privateCall(): Promise<any> {
  alert('call');
  return this.http
    .get(ENV.urlendpoint +'/api/private')
    .toPromise();
}

}
