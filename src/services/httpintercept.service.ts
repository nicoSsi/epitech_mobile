import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent ,HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import { AuthService } from './auth.service';
import { tap } from 'rxjs/operators'; 
import { AlertController, ToastController } from 'ionic-angular';

const TIME_OUT_DELAY = 16300;

@Injectable()
export class HttpinterceptService implements HttpInterceptor {
   alertIsDisplayed: boolean = false;

   constructor(public auth: AuthService
      , public alertCtrl: AlertController,
      private toastCtrl: ToastController) { }

   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      req= req.clone({
         setHeaders: {
            Authorization: `Bearer ${this.auth.accessToken}`
         }
      });
      return next.handle(req).pipe(
         tap(event => {
           if (event instanceof HttpResponse) {
           
           }
         })
       );
   }

   presentToast(str) {
      let toast = this.toastCtrl.create({
         message: str,
         duration: 3000,
         position: 'top'
      });
      toast.onDidDismiss(() => { console.log('Dismissed toast'); });
      toast.present();
   }

   showConfirm() {
      let confirm = this.alertCtrl.create({
         title: 'Déconnexion',
         message: 'Par mesure de sécurité, votre authentification doit être renouvelée. Merci de vous connecter à nouveau.',
         buttons:
            [{
               text: 'Ok',
               handler: () => {  window.location.reload(); }
            }]
      });
      confirm.present();
   }

}
