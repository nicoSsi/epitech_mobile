import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from './../../services/auth.service';
import { ApiProvider } from './../../providers/api/api';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  resultat;
  constructor(
    public navCtrl: NavController,
    public auth: AuthService,
    public _ApiProvider : ApiProvider
  ) {}


  async publicCall() {
    const response = await  this._ApiProvider.publicCall();
    console.log(response);
    this.resultat = response.message;
  }

  async privateCall() {
    const response = await  this._ApiProvider.privateCall();
    console.log(response);
    this.resultat = response.message;
  }

}
