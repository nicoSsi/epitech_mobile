export interface Environment {
  mode: string;
  urlendpoint: string;
  logouturl: string;
}