var fs = require('fs');
var figlet = require('figlet');
var fortune = require('fortune-teller');

function readWriteSync() {
  let env = (process.env.ENV || 'dev').replace(' ', '');
  consoleOut();
  var data = fs.readFileSync(`src/env/env.${env}.ts`, 'utf-8');
  fs.writeFileSync('src/env/env.ts', data, 'utf-8');
}

function consoleOut() {
  let env = (process.env.ENV || 'dev').replace(' ', '');
  let filePath = `src/env/env.${env}.ts`;
  var fortuned = fortune.fortune();
  figlet('Hello Southsider', function(err, data) {
    if (err) {
        console.log('Something went wrong...');
        console.dir(err);
        return;
    }
    console.log(data)
  });
  
  console.log("============ PREPARE ENV FILE =============");
  console.log("                                           ");
  console.log("                ["+env+"]                  ");
  console.log("             ["+filePath+"]                ");
  console.log("                                           ");
  console.log("===========================================");
  console.log("" + fortuned +"");
             
}

readWriteSync();